﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NativeWifi;
using System.Net.NetworkInformation;
using static NativeWifi.WlanClient;
using SimpleWifi;

namespace Wifi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private WlanClient _client;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {                 
                if(cbWep.IsChecked == true && cbWpa.IsChecked == true)
                {
                    MessageBox.Show("Only Check Either WPA or WEP");
                }
                else if (cbWep.IsChecked == true || cbWpa.IsChecked == true)
                {
                    Connect();
                }
                else
                {
                    MessageBox.Show("Please select WPA or WEP");
                }
        }

        private void btnScan_Click(object sender, RoutedEventArgs e)
        {
            ScanWifi();
        }

        private void btnDisconnect_Click(object sender, RoutedEventArgs e)
        {
            Disconnect();
        }

        private void lbxWifiList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(!string.IsNullOrEmpty(lbxWifiList.SelectedValue.ToString()))
                {
                txtSSID.Text = lbxWifiList.SelectedValue.ToString();               
            }
          
        }

        #region wifi core function

        public void Connect()
        {
            //get selected wifi 
            string selected = lbxWifiList.SelectedValue.ToString();

            string mac = string.Empty;
            string profileXml = string.Empty;
            _client = new WlanClient();
            try
            {


                foreach (WlanClient.WlanInterface wlanIface in _client.Interfaces)
                {
                    //check if profile exist
                    foreach (Wlan.WlanProfileInfo profileInfo in wlanIface.GetProfiles())
                    {
                        if (profileInfo.profileName == selected)
                        {
                            profileXml = wlanIface.GetProfileXml(profileInfo.profileName);
                        }
                    }

                    string profileName = selected;
                    string key = txtPassword.Text;
                    //if profile exist then connect
                    if (!string.IsNullOrEmpty(profileXml))
                    {
                        wlanIface.Connect(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, profileName);
                        MessageBox.Show("Connected to wifi");
                    }
                    else
                    {
                        string networkType = string.Empty;
                        //create profile 
                        if (cbWep.IsChecked == true)
                        {
                            networkType = "WEP";
                        }
                        else if (cbWpa.IsChecked == true)
                        {
                            networkType = "WPA";
                        }

                        Wlan.WlanBssEntry[] wlanBssEntries = wlanIface.GetNetworkBssList();
                        foreach (Wlan.WlanBssEntry wlanBssEntry in wlanBssEntries)
                        {
                            if (GetStringForSSID(wlanBssEntry.dot11Ssid) == profileName)
                            {
                                byte[] macAddr = wlanBssEntry.dot11Bssid;
                                var macAddrLen = (uint)macAddr.Length;
                                var str = new string[(int)macAddrLen];
                                for (int i = 0; i < macAddrLen; i++)
                                {
                                    str[i] = macAddr[i].ToString("x2");
                                }
                                mac = string.Join("", str);
                            }
                        }
                        if (networkType.Equals("WPA"))
                        {
                            profileXml = string.Format("<?xml version=\"1.0\"?><WLANProfile xmlns=\"http://www.microsoft.com/networking/WLAN/profile/v1\"><name>{0}</name><SSIDConfig><SSID><hex>{1}</hex><name>{0}</name></SSID><nonBroadcast>false</nonBroadcast></SSIDConfig><connectionType>ESS</connectionType><connectionMode>auto</connectionMode><autoSwitch>true</autoSwitch><MSM><security><authEncryption><authentication>WPA2PSK</authentication><encryption>AES</encryption><useOneX>false</useOneX></authEncryption><sharedKey><keyType>passPhrase</keyType><protected>true</protected><keyMaterial>{2}</keyMaterial></sharedKey></security></MSM></WLANProfile>", profileName, mac, key);
                        }
                        else if (networkType.Equals("WEP"))
                        {
                            profileXml = string.Format("<?xml version=\"1.0\"?><WLANProfile xmlns=\"http://www.microsoft.com/networking/WLAN/profile/v1\"><name>{0}</name><SSIDConfig><SSID><hex>{1}</hex><name>{0}</name></SSID></SSIDConfig><connectionType>ESS</connectionType><MSM><security><authEncryption><authentication>open</authentication><encryption>WEP</encryption><useOneX>false</useOneX></authEncryption><sharedKey><keyType>networkKey</keyType><protected>false</protected><keyMaterial>{2}</keyMaterial></sharedKey><keyIndex>0</keyIndex></security></MSM></WLANProfile>", profileName, mac, key);
                        }

                        wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, profileXml, true);
                        wlanIface.Connect(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, profileName);
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Failed to connect to wifi. Please check with again");
            }
        }

        public string FetchMacId()
        {
            string macAddresses = "";
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    macAddresses += nic.GetPhysicalAddress().ToString();
                    break;
                }
            }
            return macAddresses;
        }

        public void Disconnect()
        {

            _client = new WlanClient();
            foreach (WlanInterface wlanIface in _client.Interfaces)
            {
                wlanIface.DeleteProfile(txtSSID.Text);
            }

        }

        public void ScanWifi()
        {
            lbxWifiList.SelectedValue = "";
            lbxWifiList.Items.Clear();
             _client = new WlanClient();
            foreach (WlanClient.WlanInterface wlanInterface in _client.Interfaces)
            {
                Wlan.WlanAvailableNetwork[] networks = wlanInterface.GetAvailableNetworkList(0);
                foreach (Wlan.WlanAvailableNetwork network in networks)
                {
                    lbxWifiList.Items.Add(GetStringForSSID(network.dot11Ssid));
                }
            }
        }

        static string GetStringForSSID(Wlan.Dot11Ssid ssid)
        {
            return Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength);
        }
        #endregion
    }
}
